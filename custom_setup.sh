#/bin/bash

# Functions to install each of the packages
batInstall() {
    echo -e ""
    echo -e "\e[32mInstalling Bat..."
    echo -e "\e[39m"
    sudo apt-get update
    sudo apt-get -y install bat
    echo -e "\e[32mInstallation finished!"
    echo -e "\e[32mEnjoy it! :D"
    echo -e "\e[39m"
}

exaInstall() {
    echo -e ""
    echo -e "\e[32mInstalling Exa..."
    echo -e "\e[39m"
    sudo apt-get update
    sudo apt-get install exa
    echo -e "\e[32mInstallation finished!"
    echo -e "\e[32mEnjoy it! :D"
    echo -e "\e[39m"
}

gitInstall() {
    echo -e ""
    echo -e "\e[32mInstalling Git..."
    echo -e "\e[39m"
    sudo apt-get update
    sudo apt-get -y install git tree
    git config --global color.ui true

    echo -e "\e[32mInstallation finished!"
    echo -e "\e[32mEnjoy it! :D"
    echo -e "\e[39m"
}

tmuxInstall() {
    echo -e ""
    echo -e "\e[32mInstalling Tmux..."
    echo -e "\e[39m"
    sudo apt-get update
    sudo apt-get -y install tmux    
    
    echo -e "\e[32mInstallation finished!"
    echo -e "\e[32mEnjoy it! :D"
    echo -e "\e[39m"
}

zshInstall() {
    echo -e ""
    echo -e "\e[32mInstalling Zsh..."
    echo -e "\e[39m"
    sudo apt-get update
    sudo apt-get -y install zsh
    echo -e "\e[32mInstalling oh-my-zsh..."
    echo -e "\e[39m"
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
    rm .zshrc
    echo -e ""
    echo -e "\e[32mDownloading .zshrc configuration file..."
    echo -e "\e[39m"
    wget https://gitlab.com/linux2/my_dotfiles/-/raw/master/.zshrc
    sudo usermod --shell $(which zsh) $USER
    zsh
    source ~/.zshrc

    echo -e "\e[32mInstallation finished!"
    echo -e "Run 'zsh' to try it out."
    echo -e "\e[32mEnjoy it! :D"
    echo -e "\e[39m"
}

myAliasInstall() {
    # Set your shell: BASH or ZSH
    shell=.bashrc

    # Check current shell
    cs=`echo $SHELL | awk -F '/bin/' '{print $2}'`

    if [[ $cs != "bash" ]];then
        shell=".zshrc"
    fi

    echo -e "\e[32mInstalling my personal alias list on $shell..."
    echo -e "\e[39m"

    curl --silent https://gitlab.com/linux2/my_dotfiles/-/raw/master/myalias >> ~/$shell

    echo ""
    echo -e "\e[32mInstallation finished!"
    echo -e "\e[32mEnjoy it! :D"
    echo -e "\e[39m"
}

myHowtosInstall() {
    echo -e "\e[32mInstalling my personal \"HowTos\"..."
    echo -e "\e[39m"

    git clone https://gitlab.com/linux2/howtos.git ~/01-HowTos
    cd ~/01-HowTos
    bash install_howtos.sh
}

myTmuxInstall() {
    echo -e "\e[32mInstalling my personal \"TMUX\" configuration..."
    echo -e "\e[39m"

    wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/.tmux.conf ~/.tmux.conf
    mkdir ~/.config/tmux
    wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/config/battery.sh ~/.config/tmux/
    wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/config/docker.sh ~/.config/tmux/
    wget https://gitlab.com/linux2/general-tools/tmux/-/raw/master/config/ram.sh ~/.config/tmux/
    cd ~/.config/tmux
    chmod u+x *.*

    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    echo -e "\e[32mPress prefix + I (capital i, as in Install) to fetch the plugin."
    echo -e "\e[32mYou're good to go! The plugin was cloned to ~/.tmux/plugins/ dir and sourced."
    echo ""
    echo -e "\e[32mInstallation finished!"
    echo -e "\e[32mEnjoy it! :D"
    echo -e "\e[39m"
}

# Save current PWD
curdir=$(pwd)

# Installation Menu
whiptail --title "ESB Packages Installation Menu" --checklist --separate-output "\nSelect packages to install:" 20 78 12 \
"Bat" "A \"cat\" clone with wings." off \
"Exa" "A modern replacement for \"ls\"." off \
"Git" "Free and open source distributed version control system." off \
"Tmux" "A terminal multiplexer for Unix-like operating systems." off \
"Zsh" "The Zsh Unix shell plus my own \"dotfiles\"." off \
"MyAlias" "My own alias installation for Bash or Zsh." off \
"MyHowtos" "My personal \"HowTos\" list." off \
"MyTmux" "My personal \"TMUX\" configuration." off 2>results

while read choice
do
	case $choice in
		Bat) batInstall
		;;
		Exa) exaInstall
		;;
		Git) gitInstall
		;;
		Tmux) tmuxInstall
		;;
		Zsh) zshInstall
		;;
		MyAlias) myAliasInstall
		;;
		MyHowtos) myHowtosInstall
		;;
		MyTmux) myTmuxInstall
		;;        
		*)
		;;
	esac
done < results

# Remove "results" file
cd $curdir
rm results
