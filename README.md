# My_Dotfiles

My personal packages installation script (for Linux x86; not valid for ARM).

For ARM based dotfiles, please jump to: [My Raspberry Pi Dotfiles](https://gitlab.com/raspberrypi3/dotfiles)

# Objetivo

Este repositorio tiene la finalidad de agilizar la instalación de herramientas y programas básicos en sistema operativo Debian-like, así como sincronizar los archivos de configuración de Bash y Zsh.

# Instalación

El fichero que realiza toda la instalación es **setup.sh**.

Para correr el script de instalación con sólo un comando, ejecutaremos el siguiente comando:
```zsh
$wget -O - https://gitlab.com/linux2/my_dotfiles/-/raw/master/setup.sh | bash
```

El script instala diversos programas juntos a sus dependencias y descarga los archivos de configuración de Bash y Zsh. Además, configura **Zsh** como shell por defecto.

Programas instalados:

* Git
* Zsh
* Oh-My-Zsh
* Exa
* Bat

# Instalación Avanzada

Si en lugar de realizar una instalación completa queremos hacer una instalación "customizada", ejectuaremos el script **custom_setup.sh**. Este script nos permitira realizar las siguientes tareas:
* Instalación de los paquetes:
  * Bat
  * Exa
  * Git
  * Zsh + My_Alias
  * My_Alias
  * My_HowTos
  
Para ejecutar el script **custom_setup.sh** con solo un comando haremos lo siguiente:
```zsh
$wget -O - https://gitlab.com/linux2/my_dotfiles/-/raw/master/custom_setup.sh | bash
```

# Referencias

* https://github.com/lucashour/dotfiles

# Contributing

1. Fork it!
2. Create your feature branch: git checkout -b my-new-feature
3. Commit your changes: git commit -am 'Add some feature'
4. Push to the branch: git push origin my-new-feature
5. Submit a pull request :D
