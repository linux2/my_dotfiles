#!/bin/bash

# Change directory to $HOME
cd $HOME

echo -e "\e[32mStarting setup process..."
echo "Updating repos and dependencies..."
echo -e "\e[39m"
sudo apt-get -y update
sudo apt-get -y upgrade

# General utilities
echo -e ""
echo -e "\e[32mInstalling general utilities..."
echo -e "\e[39m"
sudo apt-get -y install git tree
git config --global color.ui true

#Exa -  A modern replacement for ls
echo -e ""
echo -e "\e[32mInstalling Exa..."
echo -e "\e[39m"
sudo apt-get install exa

#Bat - A "cat" clone with wings
echo -e ""
echo -e "\e[32mInstalling Bat..."
echo -e "\e[39m"
sudo apt-get -y install bat

#Zsh
echo -e ""
echo -e "\e[32mInstalling Zsh..."
echo -e "\e[39m"
sudo apt-get -y install zsh
echo -e "\e[32mInstalling oh-my-zsh..."
echo -e "\e[39m"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
rm .zshrc
echo -e ""
echo -e "\e[32mDownloading .zshrc configuration file..."
echo -e "\e[39m"
wget https://gitlab.com/linux2/my_dotfiles/-/raw/master/.zshrc
sudo usermod --shell $(which zsh) $USER
zsh
source ~/.zshrc

echo -e "\e[32mInstallation finished!"
echo -e "Run 'zsh' to try it out."
echo -e "\e[32mEnjoy! :D"
echo -e "\e[39m"